
<div id="footer" class="two-s-footer clearfix">
    <div class="footer-box">
        <div class="container">

            <div class="nav-footer">
                <a href="<?php echo CLIENT_URL; ?>">首页</a>
                <?php $list_return = $this->list_tag("action=category module=share pid=0"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                <a href="<?php echo $t['url']; ?>"><?php echo $t['name']; ?></a>
                <?php } } ?>

            </div>
            <div class="copyright-footer">
                <p>Copyright  2012-2999 开源程序 版权所有 &nbsp;&nbsp</p>
            </div>

        </div>
    </div>
</div>
<div class="search-form">
    <form class="sidebar-search" method="get" action="/index.php">
        <input type="hidden" name="s" value="api">
        <input type="hidden" name="c" value="api">
        <input type="hidden" name="m" value="search">
        <input type="hidden" name="dir" value="news" >
        <div class="search-form-inner tanchu">
            <div class="search-form-box">
                <input class="form-search" type="text" name="keyword" placeholder="键入搜索关键词">
                <button type="submit" id="btn-search"><i class="icon-search"></i> </button>

                <div role="navigation"  class="site-nav  primary-menu">
                    <div class="menu-fix-box" >
                        <ul id="menu-navigation" class="menu">
                            <li class="<?php if ($indexc) { ?>current-menu-ancestor current-menu-parent<?php } ?> menu-item-has-children"><a href="<?php echo CLIENT_URL; ?>">网站首页</a></li>
                            <!--调用共享栏目-->
                            <!--第一层：调用pid=0表示顶级-->
                            <?php $list_return = $this->list_tag("action=category module=share pid=0"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                            <li class="menu-item-has-children <?php if (IS_SHARE && in_array($catid, $t['catids'])) { ?> current-menu-ancestor current-menu-parent<?php } ?>">
                                <?php if ($t['child']) { ?>

                                <a href="javascript:;" title="<?php echo $t['name']; ?>"><?php echo $t['name']; ?></a>
                                <ul class="sub-menu ">
                                    <!--第二层-->
                                    <?php $list_return_t2 = $this->list_tag("action=category module=share pid=$t[id]  return=t2"); if ($list_return_t2) extract($list_return_t2, EXTR_OVERWRITE); $count_t2=dr_count($return_t2); if (is_array($return_t2)) { foreach ($return_t2 as $key_t2=>$t2) {  $is_first=$key_t2==0 ? 1 : 0;$is_last=$count_t2==$key_t2+1 ? 1 : 0; ?>
                                    <li class="<?php if (IS_SHARE && in_array($catid, $t2['catids'])) { ?> current-menu-ancestor current-menu-parent<?php } ?>">
                                        <a href="<?php echo $t2['url']; ?>" title="<?php echo $t2['name']; ?>">
                                            <?php echo $t2['name']; ?>
                                        </a>
                                    </li>
                                    <?php } } ?>
                                </ul>
                                <?php } else { ?>

                                <a href="<?php echo $t['url']; ?>" title="<?php echo $t['name']; ?>"><?php echo $t['name']; ?></a>
                                <?php } ?>
                            </li>
                            <?php } } ?>
                        </ul>
                    </div>
                </div>

            </div>


        </div>

    </form>


    <div class="close-search"> <span class="close-top"></span> <span class="close-bottom"></span> </div>


</div>

<script type='text/javascript' src='<?php echo HOME_THEME_PATH; ?>pc/js/jquery.sticky-kit.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_THEME_PATH; ?>pc/js/bootstrap.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_THEME_PATH; ?>pc/js/jquery.mcustomscrollbar.concat.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_THEME_PATH; ?>pc/js/suxingme.js'></script>
<script type='text/javascript' src='<?php echo HOME_THEME_PATH; ?>pc/js/jquery.bootstrap-autohidingnavbar.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_THEME_PATH; ?>pc/js/owl.carousel.min.js'></script>
<script type='text/javascript' src='<?php echo HOME_THEME_PATH; ?>pc/js/wow.min.js'></script>
</body>
</html>