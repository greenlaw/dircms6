<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $meta_title; ?></title>
    <meta content="<?php echo $meta_keywords; ?>" name="keywords" />
    <meta content="<?php echo $meta_description; ?>" name="description" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE;chrome=1">
    <link rel='stylesheet' id='carousel-css' href='<?php echo HOME_THEME_PATH; ?>pc/css/owl.carousel.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mcustomscrollbar-css'  href='<?php echo HOME_THEME_PATH; ?>pc/css/jquery.mcustomscrollbar.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='animate-css'  href='<?php echo HOME_THEME_PATH; ?>pc/css/animate.css' type='text/css' media='all' />
    <link rel='stylesheet' id='fontello-css'  href='<?php echo HOME_THEME_PATH; ?>pc/css/fontello.css' type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap-css'  href='<?php echo HOME_THEME_PATH; ?>pc/css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='Grace-style-css'  href='<?php echo HOME_THEME_PATH; ?>pc/css/style.css' type='text/css' media='all' />
    <script src="<?php echo THEME_PATH; ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script type='text/javascript' src='<?php echo HOME_THEME_PATH; ?>pc/js/jquery-migrate.min.js'></script>
</head>
<body class="home blog off-canvas-nav-left">
<div id="header" class=" navbar-fixed-top">
    <div class="container">
        <h1 class="logo"> <a href="<?php echo CLIENT_URL; ?>" style="background-image: url('<?php echo SITE_LOGO; ?>');"/> </a> </h1>
        <div role="navigation"  class="site-nav  primary-menu">
            <div class="menu-fix-box" >
                <ul id="menu-navigation" class="menu">
                    <li class="<?php if ($indexc) { ?>current-menu-ancestor current-menu-parent<?php } ?> menu-item-has-children"><a href="<?php echo CLIENT_URL; ?>">网站首页</a></li>
                    <!--调用共享栏目-->
                    <!--第一层：调用pid=0表示顶级-->
                    <?php $list_return = $this->list_tag("action=category module=share pid=0"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                    <li class="menu-item-has-children <?php if (IS_SHARE && in_array($catid, $t['catids'])) { ?> current-menu-ancestor current-menu-parent<?php } ?>">
                        <a href="<?php echo $t['url']; ?>" title="<?php echo $t['name']; ?>"><?php echo $t['name']; ?></a>
                        <?php if ($t['child']) { ?>
                        <ul class="sub-menu ">
                            <!--第二层-->
                            <?php $list_return_t2 = $this->list_tag("action=category module=share pid=$t[id]  return=t2"); if ($list_return_t2) extract($list_return_t2, EXTR_OVERWRITE); $count_t2=dr_count($return_t2); if (is_array($return_t2)) { foreach ($return_t2 as $key_t2=>$t2) {  $is_first=$key_t2==0 ? 1 : 0;$is_last=$count_t2==$key_t2+1 ? 1 : 0; ?>
                            <li class="<?php if (IS_SHARE && in_array($catid, $t2['catids'])) { ?> current-menu-ancestor current-menu-parent<?php } ?>">
                                <a href="<?php echo $t2['url']; ?>" title="<?php echo $t2['name']; ?>">
                                    <?php echo $t2['name']; ?>
                                </a>
                            </li>
                            <?php } } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } } ?>
                </ul>
            </div>
        </div>
        <div class="right-nav" >
            <button class="js-toggle-search"><i class=" icon-search"></i></button>
        </div>
    </div>
</div>