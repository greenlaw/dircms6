<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>
<div class="note note-danger">
    <p>迅睿CMS终身免费开源</p>
    <p style="margin-top:5px;"><a href="https://www.xunruicms.com/news/453.html" target="_blank" style="color: green;">自定义OEM版权信息，改成属于自己的CMS程序</a></p>
</div>
<form class="form-horizontal" role="form" id="myform">
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-green sbold "><?php echo dr_lang('服务器信息'); ?></span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-body fc-yun-list">


                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('上传最大值'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><a href="javascript:dr_iframe_show('show', '<?php echo dr_url('api/config'); ?>');"><?php echo @ini_get("upload_max_filesize"); ?></a></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('POST最大值'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><a href="javascript:dr_iframe_show('show', '<?php echo dr_url('api/config'); ?>');"><?php echo @ini_get("post_max_size"); ?></a></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('PHP内存上限'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><a href="javascript:dr_iframe_show('show', '<?php echo dr_url('api/config'); ?>');"><?php echo @ini_get("memory_limit"); ?></a></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('提交表单数量'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><a href="javascript:dr_iframe_show('show', '<?php echo dr_url('api/config'); ?>');"><?php echo @ini_get("max_input_vars"); ?></a></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('Web网站目录'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><?php echo WEBPATH; ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('核心程序目录'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><?php echo FCPATH; ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('附件存储目录'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><?php echo SYS_UPLOAD_PATH; ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('模板文件目录'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><?php echo TPLPATH; ?></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo dr_lang('应用程序目录'); ?></label>
                        <div class="col-md-9">
                            <div class="form-control-static"><?php echo APPSPATH; ?></div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>
</form>


<?php if ($fn_include = $this->_include("footer.html")) include($fn_include); ?>