<?php if ($fn_include = $this->_include("header2.html")) include($fn_include); ?>

<div id="page-content">
    <div class="container">
        <div class="row">

            <div class="article col-xs-12 col-sm-8 col-md-8">
                <div class="breadcrumbs" >
                    <span itemprop="itemListElement"><i class="icon-location-2"></i></span>
                    <a href='/' class='crumb'>首页</a> >
                    <?php echo dr_catpos($catid, '', true, '<a href="[url]">[name]</a> > '); ?>
                    <span>内容</span>
                </div>
                <div class="post">
                    <div class="post-title">
                        <div class="post-entry-categories">
                            <?php if (is_array($tags)) { $count_url=dr_count($tags);foreach ($tags as $name=>$url) { ?>
                            <a href="<?php echo $url; ?>" target="_blank"><?php echo $name; ?></a>
                            <?php } } ?>
                        </div>
                        <h1 class="title"><?php echo $title; ?> </h1>
                        <div class="post_icon">
                            <span  class="postauthor"><?php echo $authro; ?></span>
                            <span  class="postcat"><i class=" icon-list-2"></i><a href="<?php echo $cat['url']; ?>"><?php echo $cat['name']; ?> </a></span>
                            <span class="postclock"><i class="icon-clock-1"></i> <?php echo $updatetime; ?></span>
                            <span class="postlike"><i class="icon-eye"></i> <?php echo dr_show_hits($id); ?> </span>
                        </div>
                    </div>
                    <div class="post-content">

                        <?php echo $content; ?>
                    </div>

                </div>
                <div class="posts-cjtz content-cjtz clearfix"></div>
                <div class="next-prev-posts clearfix">
                    <div class="prev-post" >
                        <?php if ($prev_page) { ?>
                        <a href="<?php echo $prev_page['url']; ?>" class="prev has-background" > <span>上一篇</span>
                            <h4><?php echo $prev_page['title']; ?></h4>
                        </a>
                        <?php } else { ?>
                        <a href="#" class="prev has-background" > <span>上一篇</span>
                            <h4>没有了</h4>
                        </a>
                        <?php } ?>


                    </div>
                    <div class="next-post" >

                        <?php if ($next_page) { ?>
                        <a href="<?php echo $next_page['url']; ?>" class="next has-background" > <span>下一篇</span>
                            <h4><?php echo $next_page['title']; ?></h4>
                        </a>
                        <?php } else { ?>
                        <a href="#" class="next has-background" > <span>下一篇</span>
                            <h4>没有了</h4>
                        </a>
                        <?php } ?>
                    </div>
                </div>

                <div class="related-post">
                    <h3><span>猜你喜欢</span></h3>
                    <ul>
                        <?php $list_return = $this->list_tag("action=related module=MOD_DIR NOT_id=$id tag=$tag num=6"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                        <li>
                            <div class="item"> <a href="<?php echo $t['url']; ?>" title="<?php echo $t['title']; ?>">
                                <div class="overlay"></div>
                                <img class="thumbnail" src="<?php echo dr_thumb($t['thumb']); ?>">
                                <h4><span><?php echo $t['title']; ?></span></h4>
                            </a> </div>
                        </li>
                        <?php } } ?>
                    </ul>
                </div>
                <div class="clear"></div>

            </div>
            <div class="sidebar col-xs-12 col-sm-4 col-md-4">
                <div class="widget widget_suxingme_postlist">
                    <h3><span>随便看看</span></h3>
                    <ul class="recent-posts-widget">
                        <?php $list_return = $this->list_tag("action=module module=news catid=$catid order=rand num=10"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                        <li class="others">
                            <div class="image"><a href="<?php echo $t['url']; ?>"> <img src="<?php echo dr_thumb($t['thumb']); ?>" class="thumbnail"/> </a></div>
                            <div class="title">
                                <h4><a href="<?php echo $t['url']; ?>"><?php echo $t['title']; ?></a></h4>
                                <span><?php echo $t['updatetime']; ?></span> </div>
                        </li>
                        <?php } } ?>
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php if ($fn_include = $this->_include("footer2.html")) include($fn_include); ?>