<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>
<div class="note note-danger">
    <p><?php echo dr_safe_replace_path($path); ?></p>
    <?php if (IS_EDIT_TPL) { ?>
    <p style="color: red;padding-top: 5px;"><?php echo dr_lang('目前已开启可编辑文件权限，此权限风险极高，建议尽快关闭'); ?></p>
    <?php } else { ?>
    <p style="color: green;padding-top: 5px;"><?php echo dr_lang('目前没有开启可编辑文件权限，只能查看和浏览文件'); ?></p>
    <?php } ?>
</div>

<div class="right-card-box">
<form class="form-horizontal" role="form" id="myform">
<?php echo dr_form_hidden(); ?>
<div class="table-scrollable">
    <table class="table table-striped table-bordered table-hover table-checkable dataTable">
        <thead>
        <tr class="heading">
            <?php if (\Phpcmf\Service::C()->_is_admin_auth('del')) { ?>
            <th class="myselect">
                <label class="mt-table mt-checkbox mt-checkbox-single mt-checkbox-outline">
                    <input type="checkbox" class="group-checkable" data-set=".checkboxes" />
                    <span></span>
                </label>
            </th>
            <?php } ?>
            <th><?php echo dr_lang('文件名'); ?></th>
            <th><?php echo dr_lang('别名'); ?></th>
            <th style="text-align:center" width="100"><?php echo dr_lang('大小'); ?></th>
            <th width="166"><?php echo dr_lang('修改日期'); ?></th>
            <th><?php echo dr_lang('操作'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php if (is_array($list)) { $count_t=dr_count($list);foreach ($list as $t) { ?>
        <tr class="odd gradeX" id="dr_row_<?php echo $t['id']; ?>">
            <?php if (\Phpcmf\Service::C()->_is_admin_auth('del')) { ?>
            <td class="myselect">
                <label class="mt-table mt-checkbox mt-checkbox-single mt-checkbox-outline">
                    <input type="checkbox" class="checkboxes" name="ids[]" value="<?php echo $t['file']; ?>" />
                    <span></span>
                </label>
            </td>
            <?php } ?>
            <td>
                <a href="<?php echo $t['url']; ?>">
                    <img src="<?php echo $t['icon']; ?>" style="width:30px;margin-right:10px"><?php echo $t['name']; ?>
                </a>
                <?php if ($is_root && $t['thumb']) { ?>
                <a href="javascript:dr_preview_image('<?php echo $t['thumb']; ?>');" style="margin-left: 10px;">
                    <img src="<?php echo $t['thumb']; ?>" style="width:30px;">
                </a>
                <?php } ?>
            </td>
            <td><a href="<?php echo $t['cname_edit']; ?>"><?php echo $t['cname']; ?></a></td>
            <td style="text-align:center"><?php echo $t['size']; ?></td>
            <td><?php echo $t['time']; ?></td>
            <td>
                <?php if (\Phpcmf\Service::C()->_is_admin_auth('edit')) {  if ($t['edit']) { ?>
                <label><a href="<?php echo $t['edit']; ?>" class="btn btn-xs green"> <i class="fa fa-edit"></i> <?php echo dr_lang('修改'); ?></a></label>
                <?php }  if ($t['zip']) { ?>
                <label><a href="<?php echo $t['zip']; ?>" class="btn btn-xs red"> <i class="fa fa-file-zip-o"></i> <?php echo dr_lang('解压'); ?></a></label>
                <?php }  } ?>
            </td>
        </tr>
        <?php } } ?>
        </tbody>
    </table>
</div>

<div class="row fc-list-footer table-checkable ">
    <div class="col-md-5 fc-list-select">
        <?php if (\Phpcmf\Service::C()->_is_admin_auth('del')) { ?>
        <label class="mt-table mt-checkbox mt-checkbox-single mt-checkbox-outline">
            <input type="checkbox" class="group-checkable" data-set=".checkboxes" />
            <span></span>
        </label>
        <button type="button" onclick="dr_ajax_option('<?php echo $delete; ?>', '<?php echo dr_lang('你确定要删除它们吗？'); ?>', 1)" class="btn red btn-sm"> <i class="fa fa-trash"></i> <?php echo dr_lang('删除'); ?></button>
        <?php } ?>
    </div>
    <div class="col-md-7 fc-list-page">
        <?php echo $mypages; ?>
    </div>
</div>
</form></div>

<?php if ($fn_include = $this->_include("footer.html")) include($fn_include); ?>