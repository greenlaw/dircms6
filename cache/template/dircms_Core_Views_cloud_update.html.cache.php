<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>
<div class="note note-danger">
    <p>升级程序之前，请务必备份全站数据</p>
</div>

<div class="right-card-box">
<form class="form-horizontal" role="form" id="myform">
    <?php echo dr_form_hidden(); ?>
    <div class="table-scrollable">
        <table class="table table-striped table-bordered table-hover table-checkable dataTable">
            <thead>
            <tr class="heading">
                <th width="80"> 类型</th>
                <th width="250"> 程序名称</th>
                <th width="110"> 更新时间 </th>
                <th width="100"> 版本 </th>
                <th> </th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1;  if (is_array($list)) { $count_t=dr_count($list);foreach ($list as $dir=>$t) { ?>
            <tr class="odd gradeX">
                <td><?php echo $t['tname']; ?></td>
                <td><?php echo $t['name'];  if ($t['type'] == 'app') { ?> / <?php echo $dir;  } ?></td>
                <td> <?php echo $t['updatetime']; ?> </td>
                <td> <a href="javascript:dr_show_log('<?php echo $t['id']; ?>', '<?php echo $t['version']; ?>');"><?php echo $t['version']; ?></a> </td>
                <td>
                    <?php if (!$is_syy) { ?>
                    <label style="display: none" id="dr_update_<?php echo $dir; ?>">
					<button type="button" onclick="dr_update_cms('<?php echo dr_url('cloud/todo_update', ['id'=>$t['id']]); ?>', '<?php echo dr_lang('升级前请做好系统备份，你确定要升级吗？'); ?>', 1)" class="btn red btn-xs"> <i class="fa fa-cloud-upload"></i> <?php echo dr_lang('在线升级'); ?></button>
					<?php if ($dir == 'phpcmf') { ?>
					<a href="https://www.xunruicms.com/member.php?action=down&cid=<?php echo $cmf_version['id']; ?>&is_update=v<?php echo $t['version']; ?>" target="_blank" class="btn green btn-xs"> <i class="fa fa-cloud-download"></i> <?php echo dr_lang('离线下载'); ?></a>
					<?php } ?>
					</label>
                    <?php } ?>
                    <label class="dr_check_version" id="dr_row_<?php echo $dir; ?>"></label>
                </td>
            </tr>
            <?php $i++;  } } ?>
            </tbody>
        </table>
    </div>
    <div class="row fc-list-footer table-checkable ">
        <div class="col-md-12">
            <?php if (dr_is_app('beifen')) { ?>
            <label><button type="button" onclick="dr_beifen_cms('<?php echo dr_url('beifen/home/add'); ?>', '<?php echo dr_lang('你确定要备份全站文件吗？'); ?>', 1)" class="btn green btn-sm"> <i class="fa fa-copy"></i> <?php echo dr_lang('备份全站'); ?></button></label>
            <?php }  if (!$is_syy) { ?>
            <label><a href="javascript:dr_help('610');" class="btn blue btn-sm"> <i class="fa fa-book"></i> <?php echo dr_lang('离线升级程序'); ?></a></label>
            <?php } ?>
        </div>
    </div>

</form>
</div>
<script type="text/javascript">

    $(function() {
        <?php if (is_array($list)) { $count_t=dr_count($list);foreach ($list as $dir=>$t) {  if ($t['id']) { ?>
        $("#dr_row_<?php echo $dir; ?>").html("<img style='height:17px' src='<?php echo THEME_PATH; ?>assets/images/loading-0.gif'>");
        $("#dr_update_<?php echo $dir; ?>").hide();
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "<?php echo dr_url('cloud/check_version', ['id'=>$t['id'], 'version' => $t['version']]); ?>",
            success: function(json) {
                if (json.code) {
                    $("#dr_row_<?php echo $dir; ?>").html(json.msg);
                    $("#dr_update_<?php echo $dir; ?>").show();
                } else {
                    $("#dr_row_<?php echo $dir; ?>").html("<font color='red'>"+json.msg+"</font>");
                }
            },
            error: function(HttpRequest, ajaxOptions, thrownError) {
                $("#dr_row_<?php echo $dir; ?>").html("<font color='red'>网络异常，请稍后再试</font>");
            }
        });
        <?php }  } } ?>
    });

    // ajax 批量操作确认
    function dr_update_cms(url, msg, remove) {
        layer.confirm(
                msg,
                {
                    icon: 3,
                    shade: 0,
                    title: lang['ts'],
                    btn: [lang['ok'], lang['esc']]
                }, function(index){
                    layer.close(index);

                    var login_url = '<?php echo dr_url("cloud/login"); ?>';
                    layer.open({
                        type: 2,
                        title: '登录官方云账号',
                        fix:true,
                        scrollbar: false,
                        shadeClose: true,
                        shade: 0,
                        area: ['500px', '50%'],
                        btn: [lang['ok'], lang['esc']],
                        yes: function(index, layero){
                            var body = layer.getChildFrame('body', index);
                            $(body).find('.form-group').removeClass('has-error');
                            // 延迟加载
                            var loading = layer.load(2, {
                                shade: [0.3,'#fff'], //0.1透明度的白色背景
                                time: 100000000
                            });
                            $.ajax({type: "POST",dataType:"json", url: login_url, data: $(body).find('#myform').serialize(),
                                success: function(json) {
                                    layer.close(loading);
                                    if (json.code == 1) {
                                        layer.close(index);
                                        // 验证成功
                                        layer.open({
                                            type: 2,
                                            title: '升级程序',
                                            scrollbar: false,
                                            resize: true,
                                            maxmin: true, //开启最大化最小化按钮
                                            shade: 0,
                                            area: ['80%', '80%'],
                                            success: function(layero, index){
                                                // 主要用于后台权限验证
                                                var body = layer.getChildFrame('body', index);
                                                var json = $(body).html();
                                                if (json.indexOf('"code":0') > 0 && json.length < 150){
                                                    var obj = JSON.parse(json);
                                                    layer.closeAll(index);
                                                    dr_tips(0, obj.msg);
                                                }
                                            },
                                            content: url+'&'+$('#myform').serialize()
                                        });
                                    } else {
                                        $(body).find('#dr_row_'+json.data.field).addClass('has-error');
                                        dr_tips(0, json.msg);
                                    }
                                    return false;
                                },
                                error: function(HttpRequest, ajaxOptions, thrownError) {
                                    dr_ajax_alert_error(HttpRequest, ajaxOptions, thrownError)
                                }
                            });
                            return false;
                        },
                        content: login_url+'&is_ajax=1'
                    });



                });
    }

    function dr_beifen_cms(url, msg, remove) {
        layer.confirm(
                msg,
                {
                    icon: 3,
                    shade: 0,
                    title: lang['ts'],
                    btn: [lang['ok'], lang['esc']]
                }, function(index){
                    layer.close(index);
                    layer.open({
                        type: 2,
                        title: '备份程序',
                        scrollbar: false,
                        resize: true,
                        maxmin: true, //开启最大化最小化按钮
                        shade: 0,
                        area: ['80%', '80%'],
                        success: function(layero, index){
                            // 主要用于后台权限验证
                            var body = layer.getChildFrame('body', index);
                            var json = $(body).html();
                            if (json.indexOf('"code":0') > 0 && json.length < 150){
                                var obj = JSON.parse(json);
                                layer.closeAll(index);
                                dr_tips(0, obj.msg);
                            }
                        },
                        content: url
                    });
                });
    }
    
    function dr_show_log(id, v) {
        layer.open({
            type: 2,
            title: '版本日志',
            scrollbar: false,
            resize: true,
            maxmin: true, //开启最大化最小化按钮
            shade: 0,
            area: ['80%', '80%'],
            content: '<?php echo dr_url("cloud/log_show"); ?>&id='+id+'&version='+v,
            //content: 'http://www.phpcmf.net/version.php?id='+id+'&version='+v,
        });
    }

</script>


<?php if ($fn_include = $this->_include("footer.html")) include($fn_include); ?>