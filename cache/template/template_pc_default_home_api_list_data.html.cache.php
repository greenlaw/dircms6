
<?php $list_return = $this->list_tag("action=module module=news catid=$catid page=1 cache=300"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
<div class="ajax-load-con content excerpt-one">
    <div class="content-box posts-image-box">
        <div class="posts-default-title">
            <div class="post-entry-categories">
                <?php $kw=@explode(',', $t['keywords']);  if (is_array($kw)) { $count_a=dr_count($kw);foreach ($kw as $a) {  if ($a) { ?>
                <a href="<?php echo dr_get_tag_url($a); ?>" target="_blank"><?php echo $a; ?></a>
                <?php }  } } ?>
            </div>
            <h2><a href="<?php echo $t['url']; ?>" ><?php echo $t['title']; ?></a></h2>
        </div>
        <p class="focus">
            <a href="<?php echo $t['url']; ?>" class="thumbnail">
                <span class="item"><span class="thumb-span"><img src="<?php echo dr_thumb($t['thumb']); ?>" class="thumb"></span></span>
            </a>
        </p>
        <div class="posts-default-content">
            <div class="posts-text"><?php echo dr_strcut($t['description'], 100); ?></div>
            <div class="posts-default-info">
                <ul>
                    <li class="post-author">
                        <div class="avatar"><img srcset='<?php echo dr_avatar($t['uid']); ?>' class='avatar avatar-96 photo' height='96' width='96' /></div>
                        <a> <?php echo $t['author']; ?></a></li>
                    <li class="ico-cat"><i class="icon-list-2"></i> <a href="<?php echo dr_share_cat_value($t['catid'], 'url'); ?>"><?php echo dr_share_cat_value($t['catid'], 'name'); ?></a></li>
                    <li class="ico-time"><i class="icon-clock-1"></i> <?php echo $t['updatetime']; ?></li>
                    <li class="ico-like"><i class="icon-eye"></i> <?php echo $t['hits']; ?> </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php } } ?>