<?php if ($fn_include = $this->_include("header2.html")) include($fn_include); ?>

<div id="page-content" >
    <div class="top-content">
        <div class="container">
            <div class="row">
                <div class="owl-carousel top-slide-two">
                    <?php $list_return = $this->list_tag("action=module module=news thumb=1 num=5"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                    <div class="item" style="background: url('<?php echo dr_get_file($t['thumb']); ?>'); background-repeat: no-repeat;background-size: cover;background-position: center top;">
                        <div class="slider-content">
                            <div class="slider-content-item">
                                <div class="slider-cat clearfix"><?php echo dr_share_cat_value($t['catid'], 'name'); ?> </div>
                                <h2> <a class="read-slider" href="<?php echo $t['url']; ?>"><?php echo $t['title']; ?></a> </h2>
                            </div>
                        </div>
                    </div>
                    <?php } } ?>

                </div>
                <div class="hot-articles">
                    <div class="hots-content">
                        <div class="hots-headline">人气头条</div>
                        <?php $list_return = $this->list_tag("action=module module=news order=hits num=5"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                        <div class="hots-title"> <i class="icon-record-outline"></i><a href="<?php echo $t['url']; ?>"><?php echo $t['title']; ?></a> </div>
                        <?php } } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="article col-xs-12 col-sm-8 col-md-8">
                    <div class="ajax-load-box posts-con"  id="index001">
                        <?php if ($fn_include = $this->_include("api/index_data.html")) include($fn_include); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div id="ajax-load-posts">
                        <a class="button" href="javascript:void(0);"  data-page="1" data-tips="没有数据了" data-loading="" data-root_dir="" onClick="tag_arcpagelist_multi(this,'index001','index_data.html');" >点击加载更多</a>
                    </div>
                </div>

                <div class="sidebar col-xs-12 col-sm-4 col-md-4">
                    <div class="widget widget_suxingme_postlist">
                        <h3><span>随便看看</span></h3>
                        <ul class="recent-posts-widget">

                            <?php $list_return = $this->list_tag("action=module module=news order=rand num=5"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                            <li class="others">
                                <div class="image"><a href="<?php echo $t['url']; ?>"> <img src="<?php echo dr_thumb($t['thumb']); ?>" class="thumbnail"/> </a></div>
                                <div class="title">
                                    <h4><a href="<?php echo $t['url']; ?>"><?php echo $t['title']; ?></a></h4>
                                    <span><?php echo $t['updatetime']; ?></span> </div>
                            </li>
                            <?php } } ?>
                        </ul>
                    </div>

                    <div class="widget widget_suxingme_hotpost">
                        <h3><span>热门图文</span></h3>
                        <ul class="widget_suxingme_post">
                            <?php $list_return = $this->list_tag("action=module module=news thumb=1 order=rand num=5"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                            <li> <a href="<?php echo $t['url']; ?>">
                                <div class="overlay"></div>
                                <img class="thumbnail" src="<?php echo dr_thumb($t['thumb']); ?>" />
                                <div class="title">
                                    <div class="entry-meta"><span><?php echo $t['updatetime']; ?></span></div>
                                    <h4><?php echo $t['title']; ?></h4>
                                </div>
                            </a> </li>
                            <?php } } ?>
                        </ul>
                    </div>
                    <div class="widget suxingme_tag">
                        <h3><span>热门标签</span></h3>
                        <div class="widge_tags">
                            <div class="tag-items">
                                <?php $list_return = $this->list_tag("action=tag num=40"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                                <a href="<?php echo $t['url']; ?>" class="tag-item" title="点击量：<?php echo $t['hits']; ?>"><?php echo $t['name']; ?></a>
                                <?php } } ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php if ($fn_include = $this->_include("footer2.html")) include($fn_include); ?>