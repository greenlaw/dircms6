<?php if ($fn_include = $this->_include("header2.html")) include($fn_include); ?>

<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="article col-xs-12 col-sm-8 col-md-8">
                <div class="breadcrumbs" >
                    <span itemprop="itemListElement"><i class="icon-location-2"></i></span>
                    <a href='/' class='crumb'>首页</a> >
                    <?php echo dr_catpos($catid, '', true, '<a href="[url]">[name]</a> > '); ?>
                    <span>列表</span>
                </div>

                <div class="ajax-load-box posts-con"  id="list001">
                    <?php if ($fn_include = $this->_include("api/list_data.html")) include($fn_include); ?>
                </div>
                <div class="clearfix"></div>
                <div id="ajax-load-posts">
                    <a class="button" href="javascript:void(0);"  data-page="1" data-tips="没有数据了" data-loading="" data-root_dir="" onClick="tag_arcpagelist_multi(this,'list001', 'list_data.html', '&catid=<?php echo $catid; ?>');" >点击加载更多</a>
                </div>

            </div>
            <div class="sidebar col-xs-12 col-sm-4 col-md-4">
                <div class="widget widget_suxingme_postlist">
                    <h3><span>随便看看</span></h3>
                    <ul class="recent-posts-widget">
                        <?php $list_return = $this->list_tag("action=module module=news catid=$catid order=rand num=10"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=dr_count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                        <li class="others">
                            <div class="image"><a href="<?php echo $t['url']; ?>"> <img src="<?php echo dr_thumb($t['thumb']); ?>" class="thumbnail"/> </a></div>
                            <div class="title">
                                <h4><a href="<?php echo $t['url']; ?>"><?php echo $t['title']; ?></a></h4>
                                <span><?php echo $t['updatetime']; ?></span> </div>
                        </li>
                        <?php } } ?>
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php if ($fn_include = $this->_include("footer2.html")) include($fn_include); ?>